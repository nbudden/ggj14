using UnityEngine;
using System.Collections;

public class InteractibleObject : MonoBehaviour {
	
	public enum InteractionType
	{
		Pickup,
		Use,
		Possess,
		
	};
	public delegate bool NotifyInteraction();
	
	public NotifyInteraction InteractionEvent;
	public InteractionType _interactionType;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void Interact()
	{
		Debug.Log ("Interact Called");
		switch(_interactionType)
		{
		case InteractionType.Pickup:
			Pickup();
			break;
		case InteractionType.Use:
			Use ();
			break;
		case InteractionType.Possess:
			Possess();
			break;
				
		default:
			Debug.Log("Unknown type");
			break;
		}
	}
	
	public void Pickup()
	{
		if(InteractionEvent != null)
			if(InteractionEvent())		
				gameObject.SetActive(false);
		
	}
	
	public void Use()
	{
		if(InteractionEvent != null)
			if(InteractionEvent())
			{
				UseableObject obj = gameObject.GetComponent<UseableObject>();
				obj.StartInteraction();
			}
			
	}
	public void Possess()
	{
		if(InteractionEvent != null)
		{
			if(InteractionEvent())
			{
				SelectableObject obj = gameObject.GetComponent<SelectableObject>();
				obj.FireSelect();
			}
		}
	}
	
	
	
}
