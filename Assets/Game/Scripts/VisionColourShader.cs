﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
[AddComponentMenu("Image Effects/Night Vision")]

public class VisionColourShader : MonoBehaviour {
	public enum ShaderColour
	{
		Grey,
		NightVision,

	}
	// public data
	public Shader shader;
	public Color luminence;
	public float noiseFactor=0.005f;
	public ShaderColour scolour = ShaderColour.Grey;
	float colour = 0;

	//private data
	private Material mat;
	
	//-----------------------------------------
	// start 
	void Start()
	{
		mat = new Material (shader);
		switch(scolour)
		{
		case ShaderColour.Grey:
			mat.SetVector( "lum", new Vector4( luminence.r, luminence.g, luminence.b, luminence.a) );
			break;
		case ShaderColour.NightVision:
			mat.SetVector( "lum", new Vector4( luminence.g, luminence.g, luminence.g, luminence.g) );
			break;
		}
		//shader = Shader.Find( "Image Effects/Night Vision" );


		mat.SetFloat("noiseFactor", noiseFactor);
	}
	
	//-----------------------------------------
	// Called by camera to apply image effect
	void OnRenderImage (RenderTexture source, RenderTexture destination) 
	{ 	
		mat.SetFloat("time", Mathf.Sin(Time.time * Time.deltaTime));
		Graphics.Blit( source, destination, mat );
	}
	
}