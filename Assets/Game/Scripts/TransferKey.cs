using UnityEngine;
using System.Collections.Generic;

//[RequireComponent (typeof(SphereCollider))]
public class TransferKey : MonoBehaviour
{


	 public DoorTrigger.DoorKeyType _keyType = DoorTrigger.DoorKeyType.Unlocked;
	public Transform _keyObject = null;
		// Use this for initialization
		void Start ()
		{
	
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}

	public void SetKey(List<object> objs)
	{
		if(_keyObject == null)
		{
		_keyType = (DoorTrigger.DoorKeyType)objs[0];
		_keyObject =((GameObject)objs[1]).transform;

		if(GetComponent<Human>())
			GetComponent<Human>().SetKey(_keyType,true);
		}
	}
//	public void OnTriggerEnter(Collider col)
//	{
//
//
//		Debug.Log (string.Format("{0} | {1}",col.name, gameObject.name));
//		Debug.Log (col.gameObject.GetComponent<Human>());
//		Debug.Log(_keyType != DoorTrigger.DoorKeyType.Unlocked);
//		if(col.gameObject.GetComponent<Human>() && _keyType != DoorTrigger.DoorKeyType.Unlocked  &_keyObject != null)
//		{
//			_human.SetKey(_keyType,true);
//			_keyType = DoorTrigger.DoorKeyType.Unlocked;
//			_keyObject.parent = _human.transform;
//			_keyObject = null;
//
//		}
//	}
	public void DropKey()
	{
		if(_keyObject != null)
		{
		if(GetComponent<Human>())
			GetComponent<Human>().SetKey(_keyType,false);
		_keyType = DoorTrigger.DoorKeyType.Unlocked;
		if(_keyObject != null)
		_keyObject.transform.parent = null;
		_keyObject.transform.position += Vector3.up* 0.02f;
		}

	}
}

