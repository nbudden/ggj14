﻿using UnityEngine;
using System.Collections;

public class SelectableObject : MonoBehaviour {
		public delegate void SelectionEvent();
	
		public SelectionEvent _selectionTrigger;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void FireSelect()
	{
		Debug.Log ("fireselect() called");
		if(_selectionTrigger != null)
			_selectionTrigger();
	}
}
