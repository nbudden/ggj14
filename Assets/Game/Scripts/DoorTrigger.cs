﻿using UnityEngine;
using System.Collections;

public class DoorTrigger : MonoBehaviour {

	public AudioClip Rattle;
	public AudioClip Squeak;
	public GameObject Door;
	public AnimationClip animClip;
	public enum DoorKeyType
	{
		Key,
		KeyCard,
		Combo,
		Unlocked,
	}
	public DoorKeyType DoorType;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnTriggerEnter(Collider col)
	{

		Debug.Log ("TriggerEntered");
		if(col.gameObject.GetComponent<Human>())
		{

			Human hum = col.gameObject.GetComponent<Human>();
			if(DoorType == DoorKeyType.Unlocked)
			{
				ActivateDoor();
			}
			else if (hum.HasKey(DoorType))
			{
				ActivateDoor();
				hum.SetKey(DoorType, false);
			}
			else
			{
				audio.clip = Rattle;
				audio.Play();
			}

				
		}
	}
	void ActivateDoor()
	{

		gameObject.GetComponent<Animation>().Play();
		audio.clip = Squeak;
		audio.Play();
	}
}
