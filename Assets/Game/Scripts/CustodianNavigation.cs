﻿using UnityEngine;
using System.Collections;

public class CustodianNavigation : MonoBehaviour {

	public Transform [] _wayPoints;	
	public bool _started = false;
	public bool _startProcess = false;
	public bool _reversesDirection = false;
	public bool _waitsAtEnd = false;
	public float _delayAtEnd = 10.0f;
	public bool _repeat = false;
	NavMeshAgent _agent = null;
	int _counter = 1;
	int _modifier = 1;


	// Use this for initialization
	void Start () {
		_agent = GetComponent<NavMeshAgent>();

	}
	
	// Update is called once per frame
	void FixedUpdate () {

		if(_startProcess && !_started)
		{

			_agent.SetDestination(_wayPoints[0].position);

			_started = true;




		}
		if(_started)
		{
			if(!_agent.hasPath)
			{
				if(_counter >= _wayPoints.Length || _counter < 0)
				{
					if(!_reversesDirection && _repeat)
					{
						_counter = 0;
					}
					else if(!_reversesDirection)
						_counter--;
					else
					{
						_modifier *= -1;
						_counter += _modifier;
					}
				}
				_agent.SetDestination(_wayPoints[_counter].position);
				_counter+= _modifier;

			}
		}

	
	}

}
