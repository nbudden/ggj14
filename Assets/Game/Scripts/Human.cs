using UnityEngine;
using System.Collections.Generic;

public class Human : Possessible
{

	public Dictionary<DoorTrigger.DoorKeyType,bool> Keys = new Dictionary<DoorTrigger.DoorKeyType, bool>
	{
		{DoorTrigger.DoorKeyType.Key, false},
		{DoorTrigger.DoorKeyType.KeyCard, false},
		{DoorTrigger.DoorKeyType.Combo, false},

	};

	public bool _key = false;
	public bool _keyCard = false;

		// Use this for initialization
	 void  Start ()
		{

		base.Start();
			MasterController.SetToHuman();
		}
	
		// Update is called once per frame
		void Update ()
		{
//		if(Input.GetKey(KeyCode.Space))
//		{
//			GetComponent<TransferKey>().DropKey();
//		}
	}
	
	#region IPossessible implementation

	public override void Transference ()
	{
		SetCameras(true);
	}

	public bool HasKey(DoorTrigger.DoorKeyType keyType)
	{
		return Keys[keyType];
	}
	public void SetKey(DoorTrigger.DoorKeyType keyType, bool has)
	{

		if (keyType == DoorTrigger.DoorKeyType.Key)
			_key = has;
		if (keyType == DoorTrigger.DoorKeyType.KeyCard)
			_keyCard = has;
		Keys[keyType] = has;
	}




	#endregion
}

