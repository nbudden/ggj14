﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
[AddComponentMenu("Image Effects/Basic Shader")]

public class BasicShader : MonoBehaviour {

	// public data
	public Shader shader;
	public Color luminence;
	public float noiseFactor=0.005f;

	float colour = 0;

	//private data
	private Material mat;
	
	//-----------------------------------------
	// start 
	void Start()
	{
		mat = new Material (shader);

		//shader = Shader.Find( "Image Effects/Night Vision" );

	}
	
	//-----------------------------------------
	// Called by camera to apply image effect
	void OnRenderImage (RenderTexture source, RenderTexture destination) 
	{ 	
		mat.SetFloat("time", Mathf.Sin(Time.time * Time.deltaTime));
		Graphics.Blit( source, destination, mat );
	}
	
}