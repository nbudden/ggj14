using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
// THis class should use a Ray from the Camera's position out {Distance} and if it hits an object of a particular layer
// it should mark that item as Highlighted or something
public class SelectTargeted : MonoBehaviour {
	
	
	public  LayerMask _layers;
	public GameObject _highlighter;
	public Color _interactionColor;
	public Color _collectibleColor;
	public InteractibleObject _interactionObject;

	//public GameObject playerObject;
	bool _interactionInProgress = false;
	public float _interactionRange = 3;
	//public RadialProgress _interactionProgress;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		CastRay();
		if(Input.GetButton("Fire2"))
		{
			_highlighter.transform.parent = null;
			_highlighter.transform.localPosition = new Vector3(10000,10000,10000);
			_highlighter.transform.localScale = Vector3.one;
			MasterController.Possess();
			//playerObject.GetComponent<Animator>().SetBool("isPickingUp",true);
		}
	}
	
	void CastRay()
	{
		Vector3 fwd = transform.TransformDirection(Vector3.forward);
		Vector3 up1 = transform.TransformDirection(Vector3.up)*0.05f;
		Vector3 right1 = transform.TransformDirection(Vector3.right)*0.05f;
		Vector3 up2 = transform.TransformDirection(Vector3.up)*0.1f;
		Vector3 right2 = transform.TransformDirection(Vector3.right)*0.1f;



		Debug.DrawRay(transform.position, transform.forward *_interactionRange);
		Debug.DrawRay(transform.position+ up1, (fwd)*_interactionRange);
		Debug.DrawRay(transform.position-up1, fwd* _interactionRange);
		Debug.DrawRay(transform.position+right1, fwd* _interactionRange);
		Debug.DrawRay(transform.position-right1, fwd* _interactionRange);

		Debug.DrawRay(transform.position+ up2, (fwd)*_interactionRange);
		Debug.DrawRay(transform.position-up2, fwd* _interactionRange);
		Debug.DrawRay(transform.position+right2, fwd* _interactionRange);
		Debug.DrawRay(transform.position-right2, fwd* _interactionRange);
		RaycastHit rayInfo;
		if(Physics.Raycast(transform.position,fwd,out rayInfo, _interactionRange,_layers) ||
		   Physics.Raycast(transform.position+ up1,fwd,out rayInfo, _interactionRange,_layers) ||
			Physics.Raycast(transform.position - up1,fwd,out rayInfo, _interactionRange,_layers) ||
			Physics.Raycast(transform.position+right1,fwd,out rayInfo, _interactionRange,_layers) ||
		   Physics.Raycast(transform.position -right1,fwd,out rayInfo, _interactionRange,_layers)||
		   Physics.Raycast(transform.position+ up2,fwd,out rayInfo, _interactionRange,_layers) ||
		   Physics.Raycast(transform.position - up2,fwd,out rayInfo, _interactionRange,_layers) ||
		   Physics.Raycast(transform.position+right2,fwd,out rayInfo, _interactionRange,_layers) ||
		   Physics.Raycast(transform.position -right2,fwd,out rayInfo, _interactionRange,_layers)) 
		{
			GameObject go = rayInfo.collider.gameObject;
			if(go == gameObject.transform.parent.transform.parent.gameObject)
				return;
			if(go.GetComponent<InteractibleObject>())
			{
				_interactionObject = go.GetComponent<InteractibleObject>();
			}
			
			if(go.layer == LayerMask.NameToLayer("Interactible"))
			{
				_highlighter.GetComponentInChildren<MeshRenderer>().material.SetColor("_TintColor", _interactionColor);
//				if(!_interactionInProgress)
//				{
//					_interactionProgress.gameObject.SetActive(true);
//					_interactionProgress.StartTimer(3.0f);
//					_interactionInProgress = true;
//					
//				}
//				else
//				{
//					if(_interactionProgress.Finished ())
//					{
//						_interactionProgress.gameObject.SetActive(false);
//						_interactionObject.Interact();
//					}
//				}
			}
			else
			{

				_highlighter.GetComponentInChildren<MeshRenderer>().material.SetColor("_TintColor", _collectibleColor);
				if(go.GetComponent<Possessible>())
				MasterController.SetTarget(go.GetComponent<Possessible>());
			}
			_highlighter.transform.parent = rayInfo.collider.transform ;
			_highlighter.transform.localPosition = Vector3.zero;

			if(rayInfo.collider.bounds.size.magnitude < 3)
				_highlighter.transform.localScale = rayInfo.collider.bounds.size *2;
			else
			_highlighter.transform.localScale = rayInfo.collider.bounds.size;
			
		}
		else
		{
			if(_highlighter != null)
			{
			_highlighter.transform.parent = null;
			_highlighter.transform.localPosition = new Vector3(10000,10000,10000);
			_highlighter.transform.localScale = Vector3.one;
			_interactionObject = null;
//			if(_interactionInProgress)
//			{
//				_interactionInProgress = false;
//				_interactionProgress.QuitTimer();
//				_interactionProgress.gameObject.SetActive(false);
//			}
			}
			else
			{
				Debug.LogWarning("Highlighter Not Attached");
			}
		}
		
	}
}
