﻿using UnityEngine;
using System.Collections;

public class MeshGenerator : MonoBehaviour {

	GameObject myobject = null;

	// Use this for initialization
	void Start () {
		myobject = CreateHexMesh ();

		var cam = transform.parent.FindChild ("CameraRight");
		myobject.transform.parent = cam;
		myobject.transform.localPosition = Vector3.zero + cam.forward * 5.0f;

		myobject.renderer.material.shader = Shader.Find ("MoarPixelated");


	}
	
	// Update is called once per frame
	void Update () {
		//myobject.transform.position = camera.transform.position + camera.transform.forward * 5.0f;
		//myobject.transform.position =
	}


	public GameObject CreateHexMesh ()
	{
		Mesh hexMesh = new Mesh ();
		Vector3[] vertices = new Vector3[6];
		Vector2[] uv = new Vector2[6];
		for (int i=0; i < 6; i++)
		{
			vertices[i] = new Vector3();
			float angle = ((float)i)*(60.0f*Mathf.PI/180.0f);
			vertices[i].x = Mathf.Cos(angle);
			vertices[i].y = Mathf.Sin(angle);
			vertices[i].z = 0.0f;
			uv[i] = new Vector2();
			uv[i].x = Mathf.Cos(angle);
			uv[i].y = Mathf.Sin(angle);
		}
		
		int[] triangles = new int[12];
		for (int i=0; i<12; i++)
			triangles [i] = new int ();
		triangles[0] = 0;
		triangles[1] = 2;
		triangles[2] = 1;
		triangles[3] = 0;
		triangles[4] = 3;
		triangles[5] = 2;
		triangles[6] = 0;
		triangles[7] = 4;
		triangles[8] = 3;
		triangles[9] = 0;
		triangles[10] = 5;
		triangles[11] = 4;

		hexMesh.vertices = vertices;
		hexMesh.uv = uv;
		hexMesh.triangles = triangles;
		hexMesh.RecalculateBounds ();
		hexMesh.RecalculateNormals ();
		
		GameObject hexFilterObject = new GameObject ("HexMesh");
		MeshFilter mf = (MeshFilter)hexFilterObject.AddComponent (typeof(MeshFilter));
		mf.mesh = hexMesh;

		MeshRenderer mr = (MeshRenderer)hexFilterObject.AddComponent (typeof(MeshRenderer));
//		Texture2D tex = new Texture2D(1, 1);
//		tex.SetPixel(0, 0, Color.white);
//		tex.Apply();
//		mr.material.mainTexture = tex;
		mr.material.color = Color.white;

		//hexFilterObject.rigidbody.isKinematic = true;
		
		return hexFilterObject;
	}
}
