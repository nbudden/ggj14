﻿using UnityEngine;
using System.Collections;

public class MasterController  {

	static Possessible _currentEntity;
	static Possessible _target;
	static bool _possessing = false;
	Light _libraryLight;
	public static void SetToHuman()
	{
		Debug.Log ("Setting to Human");
		_currentEntity =  GameObject.FindObjectOfType<Human>();
		_currentEntity.SetCameras(true);
		_currentEntity.gameObject.layer = LayerMask.NameToLayer ("Possessed");

	//	_currentEntity =  GameObject.FindObjectOfType<Dog>();
//		_currentEntity.SetCameras(true);
	}
	public static void SetTarget(Possessible go)
	{
		if(_target == null || _target != go)
		{
		_target = go;
		
		}
		else
		{
			_target = null;
		}

	}

	public static void Possess()
	{

		//raycast from camera to determine possessible object
		if(_target != null)
		{
		if(_currentEntity is Human && !_possessing)// && _target.isPossessible)
		{
				Debug.Log ("About to Possess");
			// can possess animals	
				_currentEntity.SetCameras(false);
			_target.Transference();
				_currentEntity.gameObject.layer = LayerMask.NameToLayer ("Possessible");
				_currentEntity = _target;
				_currentEntity.gameObject.layer = LayerMask.NameToLayer ("Possessed");
			_target = null;

		}
		else if(_target is Human && !_possessing)
		{
			// can only possess self

//				if(_libraryLight == null)
//				{
//					GameObject.FindGameObjectWithTag("LibraryLight");
//				}
			_currentEntity.SetCameras(false);
				_currentEntity.gameObject.layer = LayerMask.NameToLayer ("Possessible");
			_target.Transference();
			_currentEntity = _target;
				_currentEntity.gameObject.layer = LayerMask.NameToLayer ("Possessed");
			_target = null;
		}
			_possessing = false;
		}
	}


}
