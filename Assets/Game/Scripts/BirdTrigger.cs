﻿using UnityEngine;
using System.Collections;

public class BirdTrigger : MonoBehaviour {

	public GameObject Bird;
	//public GameObject BirdModel;
	public Vector3 StartPoint;
	public Vector3 EndPoint;
	public float RunTime;

	private bool triggerFired = false;
	private float timeSinceStart = 0.0f;
	private bool windowHit = false;

	// Use this for initialization
	void Start () {
		Bird.transform.localPosition = StartPoint;
		//BirdModel.animation.playAutomatically = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (triggerFired) 
		{
			timeSinceStart += Time.deltaTime;
			var percentageDone = timeSinceStart/RunTime;
			Bird.transform.localPosition = Vector3.Lerp(StartPoint, EndPoint, percentageDone);

			if (percentageDone >= 1.0f)
			{
				Bird.GetComponent<AudioSource>().Play();
				triggerFired = false;
			}
		}
	}
	
	void OnTriggerEnter(Collider c)
	{
		triggerFired = true;
		//BirdModel.GetComponent<Animation> ().Play ();
	}

}
