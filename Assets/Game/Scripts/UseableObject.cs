﻿using UnityEngine;
using System.Collections;
// Parent class for time based interaction objects 
public class UseableObject : MonoBehaviour {
	
	public delegate void PlayInteractionAnim();
	public delegate void PlayRemoveInteractionAnim();
		
	public float _InteractionDelay = 1.0f; // in seconds
	float _timer = 0;
	bool _isInteracting = false;
	public bool _canBeUndone = false;
	public float _redoDelay = 1.0f;
	float _redoTimer = 0;
	bool _completed =false;
	bool _isUndoing = false;
	public string _interactionAnimation;
	public string _removeInteractionAnimation;
	

	// Use this for initialization
	void Start () {
		animation.Stop();
	
	}
	void FixedUpdate()
	{
		if(!_completed && _isInteracting )
		{
			if(_interactionAnimation != null && !_isUndoing)
			{
				if(!gameObject.animation.isPlaying && gameObject.animation.clip.name != _interactionAnimation)
				{
					animation.clip = animation.GetClip(_interactionAnimation);
					gameObject.animation.Play(_interactionAnimation);
				}
			}
			else if (_isUndoing && _removeInteractionAnimation != null && _canBeUndone)
			{
				
				if(!gameObject.animation.isPlaying)
				{
					gameObject.animation.Play(_removeInteractionAnimation);
				}
			}
			else
			{
				Debug.LogError("Need to assign Interaction Events!");
			}
				
			
		}
		else if (_completed && _canBeUndone && _isInteracting)
		{
			if((_redoTimer += Time.deltaTime) > _redoDelay)
			{
				_completed = false;
				_isUndoing = true;
			}
		}
		else if(!_completed && !_isInteracting)
		{
			
			if(!gameObject.animation.isPlaying && gameObject.animation.clip.name != _removeInteractionAnimation)
				{
					animation.clip = animation.GetClip(_removeInteractionAnimation);
					gameObject.animation.Play(_removeInteractionAnimation);
				}
		}
			
	}
	// Update is called once per frame
	void Update () {
		
	
	}
	public void StartInteraction()
	{
		Debug.Log ("StartInteraction Called");
		_isInteracting =true;
	}
	
	public void StopInteraction()
	{
		_isUndoing=true;
	}
	void InteractionComplete()
	{
		
	}
}
