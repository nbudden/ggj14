using UnityEngine;
using System.Collections;


// This script will highlight an object on mouseover and hover a gui object if enabled

// Some components from here:

// [url]http://forum.unity3d.com/viewtopic.php?t=8420&highlight=click+object[/url]
// 
// Attach to object you want to highlight
// This script will spawn a GUI component based on clicking an object
// You can also setup any type of response





public class Highlightobject : MonoBehaviour {
	
float holdTime = 0.5f;
Camera activeCamera ;  // attach main camera, or active camera here
Rect buttonRect;
bool showGUI = false;
float fade   = 0.0f;
string myText = "Hover Text";
int GUISizeX = 200;
int GUISizeY = 90;
private float timer = 0.0f;
private Color originalColor;
float highlightMultiply = 1.50f;
	Shader _originalShader;
	public Shader _highlighShader;
	bool _highlighting = false;
	Material _originalMaterial;
	// Use this for initialization
	void Start () {
		originalColor = renderer.material.GetColor("_TintColor");
		_originalShader = renderer.material.shader;
	}
	
	// Update is called once per frame
	void Update () {
		
		float alpha = renderer.material.GetColor("_TintColor").a;

	if (_highlighting)
	{
			renderer.material.shader = _highlighShader;
			if(GetComponentInChildren<MeshRenderer>() != null)
				GetComponentInChildren<MeshRenderer>().material.shader= _highlighShader;
	}
	else
		{
			renderer.material.shader = _originalShader;
			if(GetComponentInChildren<MeshRenderer>() != null)
				GetComponentInChildren<MeshRenderer>().material.shader= _originalShader;
		}
	
		_highlighting = false;
	
	}
	public void Highlight()
	{
		_highlighting = true;
	}
	void OnMouseDown() {

    if (!Application.isPlaying)
        return;
    
    GameObject clickedObject = gameObject;

    Debug.Log("Clicked Object =", clickedObject);

}
	void OnMouseExit() {
		
	    if (!Application.isPlaying)
	        return; 
	    showGUI = false;
	    timer = 0.0f;
	    //yield return new WaitForSeconds(0.1f);
	    
	    // Set color back to original
		renderer.material.SetColor("_TintColor",originalColor); 
//	    renderer.material.color.r = originalColor.r*highlightMultiply; 
//	    renderer.material.color.g = originalColor.g*highlightMultiply; 
//	    renderer.material.color.b = originalColor.b*highlightMultiply;
//	    renderer.material.color.b = renderer.material.color.b/highlightMultiply;
//	    renderer.material.color = originalColor;
	}
	
//	void OnMouseOver() {
//		
//	    if (!Application.isPlaying)
//	        return;
//	    timer += Time.deltaTime;
//		Color tempCol = Color.white;
//	    if(timer >=0.25f)    {   
//	       Vector3 buttonPosn = activeCamera.WorldToScreenPoint(gameObject.transform.position);
//	
//	        //Set position of GUI based on click
//	        buttonRect = new Rect(buttonPosn.x, buttonPosn.y, GUISizeX, GUISizeY);
//	        showGUI = true;
//	        timer = 0.0f;
//	    }
//	    if((timer != 0f) )   {
//	
//	        tempCol.r = originalColor.r*highlightMultiply; 
//	        tempCol.g = originalColor.g*highlightMultiply; 
//	        tempCol.b = originalColor.b*highlightMultiply;
//			tempCol.b = renderer.material.GetColor("_TintColor").b*highlightMultiply;
//			renderer.material.SetColor("_TintColor",tempCol);
//	    }   
//	
//	}
//	
//
}
