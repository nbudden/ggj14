using UnityEngine;
using System.Collections;

public abstract class Possessible : MonoBehaviour
{
	public SelectableObject _mySelectableObject = null;
	public OVRGamepadController _controller = null;
	//public OVRMainMenu _mainMenu = null;
	public OVRCameraController _cameraController = null;
	MasterController _masterController = null;
	public Transform _respawnPoint;

	public void Start()
	{


		_mySelectableObject._selectionTrigger += Transference;
		

	}
	public bool IsPossessed{ get; set;}
	public  abstract void Transference(); // plays a particular transfer effect or something
	public void SetCameras(bool amUsing)
	{

		IsPossessed = amUsing;
		_cameraController.gameObject.SetActive(amUsing);


			_mySelectableObject.enabled =amUsing;
		_controller.enabled =amUsing;

	//	_mainMenu.enabled =amUsing;

	}
	public void Respawn()
	{
		if(GetComponent<TransferKey>())
			GetComponent<TransferKey>().DropKey();
		transform.position = _respawnPoint.position;
	}

}

