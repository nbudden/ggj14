﻿Shader "Custom/MultiFacet" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_Facets ("Facets (n)", Float) = 3
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
			float4 screenPos;
		};
		
		void surf (Input IN, inout SurfaceOutput o) {
			float overlap = 0.15;
			float segments = 3;

			float2 uv_Blah = IN.uv_MainTex * float2(2,2);

			half4 c = tex2D (_MainTex, uv_Blah);
			
          	//float2 screenUV = IN.screenPos.xy / IN.screenPos.w;
          	//screenUV *= float2(8,6);
          	//o.Albedo *= tex2D (_MainTex, screenUV).rgb * 2;
			
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
