﻿Shader "Image Effects/Pixelate" {
 
Properties {
	_MainTex ("Base (RGB)", RECT) = "white" {}
}
 
SubShader {
	Pass {
		ZTest Always Cull Off ZWrite Off
		Fog { Mode off }
 
CGPROGRAM
#pragma vertex vert_img
#pragma fragment frag
#pragma fragmentoption ARB_precision_hint_fastest 
#include "UnityCG.cginc"
 
// frag shaders data
uniform sampler2D _MainTex;
uniform float4 lum;
uniform float time;
uniform float noiseFactor;
 
// frag shader
float4 frag (v2f_img i) : COLOR
{	
 
	float4 pix = tex2D(_MainTex, i.uv);
 
	//obtain luminence value
	
 
	// noise simulation
	float2 t = i.uv;		
	float x = t.x *t.y * 999999 * time;
	x = fmod(x,999) * fmod(x,999);	
	float dx = fmod(x,noiseFactor);
	float dy = fmod(x,noiseFactor);
	float4 c = tex2D(_MainTex, t+float2(dx,dy));	
    pix += c;
 
	//add lens circle effect
	//(could be optimised by using texture)
	//float dist = distance(i.uv, float2(0.5,0.5));
	//pix *= smoothstep(0.5,0.45,dist);
 
	//add rb to the brightest pixels
	//pix.rb = max (pix.rg - 0.75, 0)*4;
	//pix.g = max (pix.g - 0.75, 0)*4;
	//pix.b = max (pix.b - 0.75, 0)*4;
 
	// return pix pixel ;)	
	return pix;
}
ENDCG
 
	}
}
 
Fallback off
 
}