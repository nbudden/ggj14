﻿using UnityEngine;
using System.Collections;

public class PasstriggerUp : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnTriggerEnter(Collider col)
	{
		if(col.gameObject.layer == LayerMask.NameToLayer("Possessible"))
			SendMessageUpwards("OnTriggerEnter",col,SendMessageOptions.RequireReceiver);
	}
}
