﻿using UnityEngine;
using System.Collections;

public class AnimTransition : MonoBehaviour {

    protected Animator animator;

	// Use this for initialization
    void Start()
    {
        animator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Alpha1))
        {
            animator.SetBool("Walking", false);
        }
        if (Input.GetKey(KeyCode.Alpha2))
        {
            animator.SetBool("Walking", true);
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            animator.SetBool("Action", true);
        }
		if (Input.GetKeyUp(KeyCode.E))
		{
			animator.SetBool("Action", false);
		}
	}
}
